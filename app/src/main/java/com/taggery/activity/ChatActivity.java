package com.taggery.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.taggery.R;
import com.taggery.fragment.AddChatFragment;
import com.taggery.fragment.ChatScreenFragment;
import com.taggery.utils.TaggeryConstants;

public class ChatActivity extends BaseActivity {

    String channelname,fcmtoken,userimage;
    int channelId;
    boolean isFromFloating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);

        Intent intent = getIntent();
        if (intent!=null){
            isFromFloating = intent.getBooleanExtra("isFromFloating",false);

            if (isFromFloating){
                AddChatFragment addChatFragment = new AddChatFragment();
                push(addChatFragment);

            }
            else {
                channelId = intent.getIntExtra(TaggeryConstants.CHATWITH_USERID,0);
                channelname = intent.getStringExtra(TaggeryConstants.CHAT_WITH);
                fcmtoken = intent.getStringExtra(TaggeryConstants.CHAT_FCMTOKEN);
                userimage = intent.getStringExtra(TaggeryConstants.CHAT_USERPIC);
                ChatScreenFragment chatScreenFragment = new ChatScreenFragment();
                Bundle bundle = new Bundle();
                if (channelId !=0){
                    bundle.putInt(TaggeryConstants.CHATWITH_USERID, channelId);
                    bundle.putString(TaggeryConstants.CHAT_WITH, channelname);
                    bundle.putString(TaggeryConstants.CHAT_FCMTOKEN,fcmtoken);
                    bundle.putString(TaggeryConstants.CHAT_USERPIC,userimage);
                }
                chatScreenFragment.setArguments(bundle);
                push(chatScreenFragment);

            }

        }


    }


    public void popBackStackImmediate() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.executePendingTransactions();
    }

    public void popAllBackstack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backCount = fragmentManager.getBackStackEntryCount();
        if (backCount > 0) {
            fragmentManager.popBackStackImmediate(fragmentManager.getBackStackEntryAt(0).getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_chat_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_chat_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_chat_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.frame_chat_container, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }



}

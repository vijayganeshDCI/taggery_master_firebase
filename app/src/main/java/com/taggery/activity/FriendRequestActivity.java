package com.taggery.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;

import com.taggery.R;
import com.taggery.activity.BaseActivity;
import com.taggery.adapter.FriendRequestListAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.ContactListInputParam;
import com.taggery.model.ProfileResponse;
import com.taggery.model.SuggestionItem;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FriendRequestActivity extends BaseActivity {

    @BindView(R.id.image_app_logo)
    ImageView imageAppLogo;
    @BindView(R.id.text_label_friend_req)
    CustomTextView textLabelFriendReq;
    @BindView(R.id.list_friend_req)
    ListView listFriendReq;
    @BindView(R.id.swipe_friend_req_list)
    SwipeRefreshLayout swipeFriendReqList;
    @BindView(R.id.image_warn_icon)
    ImageView imageWarnIcon;
    @BindView(R.id.text_warn)
    CustomTextView textWarn;
    @BindView(R.id.cons_warn)
    ConstraintLayout consWarn;
    @BindView(R.id.cons_friend_req)
    ConstraintLayout consFriendReq;
    Unbinder unbinder;
    FriendRequestListAdapter friendRequestListAdapter;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    ProfileResponse profileResponse;
    ArrayList<SuggestionItem> friendRequestArrayList;
    private boolean isLoading = false, reachedLastEnd = false;
    private int paginationCount = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_friend_request);
        unbinder = ButterKnife.bind(this);
        friendRequestArrayList = new ArrayList<SuggestionItem>();
        TaggeryApplication.getContext().getComponent().inject(this);
        friendRequestListAdapter = new FriendRequestListAdapter(
                friendRequestArrayList, FriendRequestActivity.this);
        listFriendReq.setAdapter(friendRequestListAdapter);
        getFriendRequestList(1);
        swipeFriendReqList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeFriendReqList.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getFriendRequestList(1);
                        swipeFriendReqList.setRefreshing(false);
                    }
                }, 000);
            }
        });

        listFriendReq.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (!isLoading && !reachedLastEnd) {
                    if ((visibleItemCount + firstVisibleItem) >= totalItemCount
                            && firstVisibleItem >= 0
                            && totalItemCount >= 29) {
                        getFriendRequestList(++paginationCount);
                    }
                }
            }
        });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        isLoading = false;
        reachedLastEnd = false;
        unbinder.unbind();
    }


    public void getFriendRequestList(final int paginationCount) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            isLoading = false;
            if (paginationCount == 1)
                friendRequestArrayList.clear();
            ContactListInputParam contactListInputParam = new ContactListInputParam();
            contactListInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            contactListInputParam.setPage(paginationCount);
            taggeryAPI.getFriendRequestList(contactListInputParam).enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                    hideProgress();
                    isLoading = false;
                    profileResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (profileResponse.getStatusCode() == 200) {
                            if (profileResponse.getOurfriendrequests() != null &&
                                    profileResponse.getOurfriendrequests().size() > 0) {
                                listFriendReq.setVisibility(View.VISIBLE);
                                consWarn.setVisibility(View.GONE);
                                friendRequestArrayList.addAll(profileResponse.getOurfriendrequests());
                                friendRequestListAdapter.notifyDataSetChanged();
                            } else {
                                listFriendReq.setVisibility(View.GONE);
                                consWarn.setVisibility(View.VISIBLE);
                                textWarn.setText(getString(R.string.no_new_friend_request));
                                imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                            }
                        } else if (profileResponse.getStatusCode() == 501) {
                            if (paginationCount != 1) {
                                reachedLastEnd = true;
                                showShackError(profileResponse.getMessage() != null ?
                                        profileResponse.getMessage() : "", consFriendReq);
                            } else {
                                listFriendReq.setVisibility(View.GONE);
                                consWarn.setVisibility(View.VISIBLE);
                                textWarn.setText(getString(R.string.server_error));
                                imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                            }
                        } else {
                            listFriendReq.setVisibility(View.GONE);
                            consWarn.setVisibility(View.VISIBLE);
                            textWarn.setText(getString(R.string.server_error));
                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                        }
                    } else {
                        listFriendReq.setVisibility(View.GONE);
                        consWarn.setVisibility(View.VISIBLE);
                        textWarn.setText(getString(R.string.server_error));
                        imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                    }

                }

                @Override
                public void onFailure(Call<ProfileResponse> call, Throwable t) {
                    hideProgress();
                    isLoading = false;
                    listFriendReq.setVisibility(View.GONE);
                    consWarn.setVisibility(View.VISIBLE);
                    textWarn.setText(getString(R.string.server_error));
                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                }
            });
        } else {
            listFriendReq.setVisibility(View.GONE);
            consWarn.setVisibility(View.VISIBLE);
            textWarn.setText(getString(R.string.no_network));
            imageWarnIcon.setImageResource(R.mipmap.icon_no_network);
        }
    }

}

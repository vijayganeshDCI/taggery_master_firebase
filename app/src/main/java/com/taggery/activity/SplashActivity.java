package com.taggery.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.taggery.BuildConfig;
import com.taggery.R;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.ContactListInputParam;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.taggery.application.TaggeryApplication.getContext;


public class SplashActivity extends AppCompatActivity {


    @BindView(R.id.relative_splash)
    RelativeLayout relativeSplash;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        TaggeryApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        editor.putString(TaggeryConstants.DEVICEID, Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID)).commit();
        editor.putString(TaggeryConstants.APPID, getApplication().getPackageName()).commit();
        editor.putString(TaggeryConstants.APPVERSION, BuildConfig.VERSION_NAME).commit();
        onlineStatus();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for sInstance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (sharedPreferences.getInt(TaggeryConstants.LOGIN_STATUS, 0) == 0) {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();

                }

            }
        }, 1000);

    }

    private void onlineStatus(){
        ContactListInputParam contactListInputParam=new ContactListInputParam();
        contactListInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID,0));
        taggeryAPI.onlineStatus(contactListInputParam).enqueue(new Callback<CheckUserNameResponse>() {
            @Override
            public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {

            }

            @Override
            public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {

            }
        });
    }


}

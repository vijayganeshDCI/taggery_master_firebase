package com.taggery.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.taggery.R;
import com.taggery.activity.PreviewActivity;
import com.taggery.fragment.PreviewFragment;

public class CustomPagerAdapter extends PagerAdapter {

    Context mContext;
    Integer images[];
    LayoutInflater mLayoutInflater;
    PreviewActivity previewActivity;
    PreviewFragment fragment;

    public CustomPagerAdapter(Context context, Integer images [], PreviewFragment fragment) {
        this.mContext = context;
        this.images = images;
        this.fragment=fragment;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.viewpager_gallerypreview, container, false);

        previewActivity = (PreviewActivity) mContext;

        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_background);
        imageView.setImageResource(images[position]);
        container.addView(itemView);

//        //listening to image click
//        imageView.setOnTouchListener( new OnSwipeTouchListener(mContext){
//            public void onSwipeTop() {
//                StatsFragment statsFragment = new StatsFragment();
//                previewActivity.push(statsFragment, "StatsFragment");
//                //Toast.makeText(mContext, "top", Toast.LENGTH_SHORT).show();
//            }
//            public void onSwipeBottom() {
//                Intent intent = new Intent(mContext, MainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
//                ((PreviewActivity) mContext).finish();
//                mContext.startActivity(intent);
//                previewActivity.overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
//               // Toast.makeText(mContext, "bottom", Toast.LENGTH_SHORT).show();
//            }
//            public void onClick() {
//                fragment.showGloss();
//            }
//
//        });
        return itemView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}

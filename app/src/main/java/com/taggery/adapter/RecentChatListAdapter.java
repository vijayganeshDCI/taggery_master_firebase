package com.taggery.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.taggery.R;
import com.taggery.model.ChatMessageParams;
import com.taggery.view.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class RecentChatListAdapter extends BaseAdapter {

    public RecentChatListAdapter(Context context, List<ChatMessageParams> contactLists) {
        this.context = context;
        this.contactLists = contactLists;
        mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    Context context;
    List<ChatMessageParams> contactLists;
    LayoutInflater mInflater;


    @Override
    public int getCount() {
        return contactLists.size();
    }

    @Override
    public ChatMessageParams getItem(int position) {
        return contactLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.childlayout_friendlist, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        universalImageLoader(viewHolder.imgProfile,
                contactLists.get(position).getReceiverPhotoUrl(),
                R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                context.getString(R.string.cloudinary_download_profile_picture));

//        Picasso.get().load(context.getString(R.string.cloudinary_base_url) +
//                context.getString(R.string.cloudinary_download_profile_picture) + contactLists.get(position).getReceiverPhotoUrl())
//                .placeholder(R.mipmap.icon_profile_place_holder).error(R.mipmap.icon_profile_place_holder).into(viewHolder.imgProfile);

        viewHolder.textFriendname.setText(contactLists.get(position).getToName());
        if (contactLists.get(position).getText()==null) {
            viewHolder.textFriendposition.setText("image");
        } else {
            viewHolder.textFriendposition.setText(contactLists.get(position).getText());
        }
        if (contactLists.get(position).getReadStatus() == 0) {
            viewHolder.imgSeen.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imgSeen.setVisibility(View.GONE);
        }


        return convertView;
    }



    static class ViewHolder {
        @BindView(R.id.img_profile)
        CircleImageView imgProfile;
        @BindView(R.id.text_friendname)
        CustomTextView textFriendname;
        @BindView(R.id.text_friendposition)
        CustomTextView textFriendposition;
        @BindView(R.id.img_seen)
        ImageView imgSeen;
        @BindView(R.id.relative_parent)
        RelativeLayout relativeParent;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void universalImageLoader(final ImageView imageViewProPic,
                                     String imageName, int loadingImage,
                                     int emptyURIImage, String imageUrl) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyURIImage)
                .showImageOnFail(emptyURIImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.cloudinary_base_url) + imageUrl + imageName,
                imageViewProPic, options);
    }
}

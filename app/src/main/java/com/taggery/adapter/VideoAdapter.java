package com.taggery.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.taggery.R;
import com.taggery.activity.ImageshowActivity;
import com.taggery.model.Model_Video;
import com.taggery.utils.TaggeryConstants;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder>{


    private ArrayList<String> imagelist;
    ArrayList<Model_Video> al_video;
    private Context context;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    SharedPreferences share;
    SharedPreferences.Editor edit;

    public VideoAdapter(ArrayList<Model_Video> al_video, Context context) {
        this.al_video = al_video;
        this.context = context;
    }


    @Override
    public VideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.childlayout_galleryitem, parent, false);

        return new VideoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.img_play.setVisibility(View.VISIBLE);
        final Model_Video model_video = al_video.get(position);

        Glide.with(context).load(model_video.getStr_thumb())
                .into(holder.icon);
        final String mediaName ="VID" + System.currentTimeMillis();
        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = new File(model_video.getStr_path());
                long length = file.length();
                length = length/2048;
                if (length > 20480){
                    Toast.makeText(context, "Select file size less than 20 MB", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(context, ImageshowActivity.class);
                    intent.putExtra("mediaPath", model_video.getStr_path());
                    intent.putExtra("mediaFilename",mediaName);
                    intent.putExtra("isImage",false);
                    edit.putString(TaggeryConstants.CHECK_GALLERY, "galley").apply();
                    context.startActivity(intent);
                }

            }
        });
    }


    @Override
    public int getItemCount() {
        return al_video.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder
    {  ImageView icon,img_play;

        public ViewHolder(View view)
        {
            super(view);
            icon = (ImageView) view.findViewById(R.id.galleyitem);
            img_play = view.findViewById(R.id.img_play);
            sharedPreferences = context.getSharedPreferences("myprefs", Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
            share = context.getSharedPreferences("mytaggery",Context.MODE_PRIVATE);
            edit = share.edit();


        }
    }
}

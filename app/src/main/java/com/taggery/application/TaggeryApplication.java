package com.taggery.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

//import com.amazonaws.mobile.client.AWSMobileClient;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.taggery.BuildConfig;
import com.taggery.dagger.AppModule;
import com.taggery.dagger.ApplicationComponent;

import com.taggery.dagger.DaggerApplicationComponent;
import com.taggery.retrofit.RetrofitModule;

import javax.inject.Inject;



public class TaggeryApplication extends MultiDexApplication {
    @Inject
    public SharedPreferences mPrefs;
    private static TaggeryApplication mInstance;


    public static TaggeryApplication getContext() {
        return mInstance;
    }

    private ApplicationComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mComponent = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .retrofitModule(new RetrofitModule(BuildConfig.TAGGERY_BASE_URL))
                .build();
        mComponent.inject(this);


        // UNIVERSAL IMAGE LOADER SETUP
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration imageLoaderConfiguration = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .diskCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(imageLoaderConfiguration);

        //AWS s3
//        AWSMobileClient.getInstance().initialize(this).execute();
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }

    public static TaggeryApplication from(@NonNull Context context) {
        return (TaggeryApplication) context.getApplicationContext();
    }
    public static TaggeryApplication get()
    {
        return mInstance;
    }


    public void showToast(final String text) {
        showToast(text, Toast.LENGTH_SHORT);
    }

    public void showToast(final String text, final int duration)
    {
        Log.d("TaggeryApplication", text);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(getApplicationContext(), text, duration);
                toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }
        });
    }



}

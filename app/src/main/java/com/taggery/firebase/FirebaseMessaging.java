package com.taggery.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;


import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.RemoteMessage;
import com.taggery.R;

import com.taggery.activity.AnalyticalModuleActivity;
import com.taggery.activity.MainActivity;
import com.taggery.utils.TaggeryConstants;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Map;

/**
 * Created by vijayaganesh on 11/13/2017.
 */

public class FirebaseMessaging extends com.google.firebase.messaging.FirebaseMessagingService {

    Intent intent;
    private NotificationManager notificationManager;
    String message = "", imageUri = "", title = "", deviceToken, typeID;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Bitmap bitmap;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                deviceToken = instanceIdResult.getToken();
                Log.i("fcmtoken", deviceToken);
                sharedPreferences = getSharedPreferences(TaggeryConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
                editor = sharedPreferences.edit();
                editor.putString(TaggeryConstants.FCMTOKEN, deviceToken).apply();
            }
        });

    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //Calling method to generate notification
        if (remoteMessage.getData().get("title") != null)
            title = remoteMessage.getData().get("title");
        if (remoteMessage.getData().get("message") != null)
            message = remoteMessage.getData().get("message");
        if (remoteMessage.getData().get("typeID") != null)
            typeID = remoteMessage.getData().get("typeID");
//        bitmap = getBitmapfromUrl(imageUri);
        sendNotification(message, bitmap, title);
    }


    private void sendNotification(String msg, Bitmap bitmap, String title) {

        String channelId = "TAGGERY_CHANNEL_ID";
        String channelName = "Taggery Notification";
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }
        if (typeID != null && typeID.equals("1")) {
            intent = new Intent(this, MainActivity.class);
            intent.putExtra("typeID", 1);
        } else
            intent = new Intent(this, AnalyticalModuleActivity.class);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.icon_app_logo);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setLargeIcon(largeIcon)
                .setSmallIcon(R.mipmap.icon_app_logo_50)
                .setContentTitle(title)
                .setContentText(msg)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        int notificationID = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        notificationManager.notify(notificationID, notificationBuilder.build());

        //        //BroadCast Receiver to notification count
//        Intent intentNo = new Intent("notificationListener");
//        intentNo.putExtra("notify", true);
//        sendBroadcast(intentNo);
    }


    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }


    @Override
    public void onCreate() {
        super.onCreate();

    }
}

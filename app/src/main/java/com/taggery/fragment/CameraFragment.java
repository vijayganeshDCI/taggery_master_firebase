package com.taggery.fragment;

/**
 * Created by iyyapparajr on 4/27/2018.
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.taggery.R;
import com.taggery.adapter.ImageRecycleAdapter;
import com.taggery.adapter.VideoAdapter;
import com.taggery.model.Model_Video;
import com.taggery.model.OnSwipeTouchListener;
import com.taggery.view.CameraPreview;
import com.taggery.view.CustomTextView;

import java.io.File;
import java.util.ArrayList;

import static android.content.Context.VIBRATOR_SERVICE;

public class CameraFragment extends BaseFragment {

    ViewGroup viewgroup;
    Fragment fragment;
    LinearLayout relative_top;
    ImageView imgswitch, flash;
    private Camera mCamera;
    private MediaRecorder mMediaRecorder;
    ImageView button_capture;
    CustomTextView text_galleryimage,text_galleryvideo;
    FrameLayout preview;
    private static final int PERMISSION_REQUEST_CODE = 200;
    String mCurrentPhotoPath;
    RecyclerView recyclerview_image;
    private ArrayList<String> images;
    private static final int FOCUS_AREA_SIZE = 300;
    CameraPreview cameraPreview;
    Vibrator vibrator;
    ImageRecycleAdapter adapter;
    VideoAdapter videoAdapter;
    BottomSheetBehavior sheetBehavior;
    RelativeLayout bottom_sheet;
    boolean sheetclick = false,backpress=false;
    private boolean isStarted;
    private boolean isVisible;
    ArrayList<Model_Video> al_video = new ArrayList<>();


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            if (mCamera == null) {
                mCamera = getCameraInstance();
                cameraPreview = new CameraPreview(getActivity(), mCamera, button_capture);
                preview.addView(cameraPreview);

            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        //returning our layout file
        View view = inflater.inflate(R.layout.camerafrag, container, false);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getActivity().getWindow(); // in Activity's onCreate() for sInstance
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }*/


        checkCameraHardware(getActivity());
        cameraPreview = new CameraPreview(getActivity(), mCamera, view);
        // mPreview = new CameraPreview(getActivity(), mCamera,rotation);
        preview = (FrameLayout) view.findViewById(R.id.camera_preview);
        text_galleryimage = view.findViewById(R.id.text_galleryimage);
        text_galleryvideo = view.findViewById(R.id.text_galleryvideo);
        preview.addView(cameraPreview);
        // preview.addView(mPreview);
//        checkCameraPermissionAboveMarshmallow();
        flash = view.findViewById(R.id.img_flash);
        button_capture = view.findViewById(R.id.img_capture);
        imgswitch = view.findViewById(R.id.img_switch);
        bottom_sheet = view.findViewById(R.id.bottom_sheet);
      //  fab = view.findViewById(R.id.fab);
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        sheetBehavior.setPeekHeight(0);
        recyclerview_image = view.findViewById(R.id.recyclerview_image);
        relative_top = view.findViewById(R.id.relative_top);
        vibrator = (Vibrator) getActivity().getSystemService(VIBRATOR_SERVICE);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                /*if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (keyEvent.getAction()!=KeyEvent.ACTION_DOWN)
                        return true;
                    else
                    {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        return true; // pretend we've processed it
                    }
                }
                else
                return false;*/


                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    if (backpress){
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                    }
                    else {
                        getActivity().finish();
                    }

                   /* if (backpress){
                        getActivity().finish();
                    }
                    else {
                        if (keyEvent.getAction()!=KeyEvent.ACTION_DOWN)
                            return true;
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        backpress=true;
                    }*/

                }
                return true;
            }
        });

        initializeListener();
        TagFragment.tagPeopleStaticSelectedList.clear();
        getVideos();
        text_galleryvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getVideos();
            }
        });
        text_galleryimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getImages();
            }
        });

        getImages();
        return view;

    }

    private void getImages() {
        Uri uri;
        Cursor cursor;
        String orderBy = MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC";
        String[] columns = {MediaStore.Images.Media._ID,
                MediaStore.Images.ImageColumns.DATE_TAKEN};
        int column_index_data, column_index_folder_name;
        ArrayList<String> listOfAllImages = new ArrayList<String>();
        String absolutePathOfImage = null;
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME};
        cursor = getActivity().getContentResolver().query(uri, projection, null,
                null, orderBy);
        column_index_data = cursor.getColumnIndexOrThrow
                (MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);

            listOfAllImages.add(absolutePathOfImage);
        }
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerview_image.setLayoutManager(mLayoutManager);
        recyclerview_image.setItemAnimator(new DefaultItemAnimator());
        adapter = new ImageRecycleAdapter(listOfAllImages, getActivity());
        recyclerview_image.setAdapter(adapter);
        ViewCompat.setNestedScrollingEnabled(recyclerview_image, true);

    }

    @Override
    public void onResume() {
        super.onResume();
        //  manager.registerListener(this, rotation_vector, SensorManager.SENSOR_DELAY_NORMAL);
        cameraPreview.resumeMethod();

    }


    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            if (mCamera == null) {
                mCamera = getCameraInstance();
                cameraPreview = new CameraPreview(getActivity(), mCamera, button_capture);
                preview.addView(cameraPreview);
            }
        }

    }



    @SuppressLint("ClickableViewAccessibility")
    private void initializeListener() {

        preview.setOnTouchListener(new OnSwipeTouchListener(getActivity()){
            public void onSwipeTop() {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
            public void onSwipeBottom() {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
            public void onClick(MotionEvent event) {
                cameraPreview.focusOnTouch(event);

            }

        });

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        backpress = true;

                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        backpress =false;

                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                //fab.animate().scaleX(1 - slideOffset).scaleY(1 - slideOffset).setDuration(0).start();

            }
        });
        relative_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sheetclick) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    sheetclick = false;
                    backpress =false;
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    sheetclick = true;
                    backpress=true;
                }

            }
        });
        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });*/

        imgswitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraPreview.switchCamera();
            }
        });
        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cameraPreview.flashOpen();
            }
        });


    }



    private void getVideos() {
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name,column_id,thum;

        String absolutePathOfImage = null;
        uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Video.Media.BUCKET_DISPLAY_NAME,MediaStore.Video.Media._ID,MediaStore.Video.Thumbnails.DATA};

        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
        cursor = getActivity().getContentResolver().query(uri, projection, null, null, orderBy + " DESC");

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.BUCKET_DISPLAY_NAME);
        column_id = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
        thum = cursor.getColumnIndexOrThrow(MediaStore.Video.Thumbnails.DATA);

        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            Log.e("Column", absolutePathOfImage);
            Log.e("Folder", cursor.getString(column_index_folder_name));
            Log.e("column_id", cursor.getString(column_id));
            Log.e("thum", cursor.getString(thum));

            Model_Video obj_model = new Model_Video();
            obj_model.setBoolean_selected(false);
            obj_model.setStr_path(absolutePathOfImage);
            obj_model.setStr_thumb(cursor.getString(thum));

            al_video.add(obj_model);

        }

        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerview_image.setLayoutManager(mLayoutManager);
        recyclerview_image.setItemAnimator(new DefaultItemAnimator());
        videoAdapter = new VideoAdapter (al_video, getActivity());
        recyclerview_image.setAdapter(videoAdapter);
        ViewCompat.setNestedScrollingEnabled(recyclerview_image, true);
    }


    /*private ArrayList<String> getAllShownImagesPath(Context context) {
        Uri uri;
        Cursor cursor;
        String orderBy = MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC";
        String[] columns = {MediaStore.Images.Media._ID,
                MediaStore.Images.ImageColumns.DATE_TAKEN};
        int column_index_data, column_index_folder_name;
        ArrayList<String> listOfAllImages = new ArrayList<String>();
        String absolutePathOfImage = null;
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME};
        cursor = context.getContentResolver().query(uri, projection, null,
                null, orderBy);
        column_index_data = cursor.getColumnIndexOrThrow
                (MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);

            listOfAllImages.add(absolutePathOfImage);
        }
        return listOfAllImages;




    }*/


    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            mCamera = getCameraInstance();
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }




    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }



    @TargetApi(Build.VERSION_CODES.M)
    void showPermissionDialog() {
        try {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
                    android.Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

}

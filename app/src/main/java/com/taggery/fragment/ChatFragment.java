package com.taggery.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.taggery.R;
import com.taggery.activity.ChatActivity;
import com.taggery.adapter.RecentChatListAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.ChatMessageParams;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ChatFragment extends BaseFragment {


    @BindView(R.id.list_user_list)
    ListView listUserList;
    @BindView(R.id.floating_add)
    FloatingActionButton floatingAdd;
    @BindView(R.id.swipe_chat)
    SwipeRefreshLayout swipeChat;
    Unbinder unbinder;
    @BindView(R.id.image_no_network)
    ImageView imageNoNetwork;
    @BindView(R.id.text_no_network)
    CustomTextView textNoNetwork;
    @BindView(R.id.relative_info)
    RelativeLayout relativeInfo;
    private boolean isStarted;
    private boolean isVisible;

    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    private String senderUserName;
    private int senderUserID;
    public String RECENT_LIST_CHILD;
    private DatabaseReference mFirebaseDatabaseReferenceInbox;
    public String RECENT_CHAT = "RECENT_CHAT_MESSAGES";
    private DatabaseReference messagesRef;
    private ArrayList<ChatMessageParams> membersReceiverList;
    RecentChatListAdapter reecentChatListAdapter;
    public String ONE_TO_ONE_CHAT_MES_CHILD = "ONE_TO_ONE_CHAT_MESSAGES";
    public String MESSAGES_CHILD_SENDER, MESSAGES_CHILD_RECEIVER;


    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            getInboxData();
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            getInboxData();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        unbinder = ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        listUserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int postion, long l) {
                Intent intent = new Intent(getActivity(), ChatActivity.class);

                intent.putExtra(TaggeryConstants.CHAT_WITH, membersReceiverList.get(postion).getToName());
                intent.putExtra(TaggeryConstants.CHATWITH_USERID, membersReceiverList.get(postion).getChatWithID());
                intent.putExtra(TaggeryConstants.CHAT_FCMTOKEN, membersReceiverList.get(postion).getReceiverFcmToken());
                intent.putExtra(TaggeryConstants.CHAT_USERPIC,membersReceiverList.get(postion).getReceiverPhotoUrl());

                startActivity(intent);

                messagesRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        //Old User
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            dataSnapshot.getRef().child("readStatus").setValue(1);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });


        return view;
    }

    private void getInboxData() {
        membersReceiverList = new ArrayList<ChatMessageParams>();
        senderUserName = sharedPreferences.getString(TaggeryConstants.USER_NAME, "");
        senderUserID = sharedPreferences.getInt(TaggeryConstants.USER_ID, 0);
        RECENT_LIST_CHILD = "" + senderUserID;
        showProgress();
        mFirebaseDatabaseReferenceInbox = FirebaseDatabase.getInstance().getReference();
        messagesRef = mFirebaseDatabaseReferenceInbox.child(RECENT_CHAT).child("Inbox")
                .child(RECENT_LIST_CHILD);
        messagesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                hideProgress();
                Log.i("snapshot", snapshot.toString());
                if (snapshot.getChildrenCount() > 0) {
                    Log.i("snapshot", "" + snapshot.getChildrenCount());
                    listUserList.setVisibility(View.VISIBLE);
                    relativeInfo.setVisibility(View.INVISIBLE);
                    membersReceiverList.clear();
                    try {
                        for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                            Log.i("snapshot1", "" + postSnapshot.getValue());
                            ChatMessageParams chatMessageParams = postSnapshot.getValue(ChatMessageParams.class);
                            Log.i("chatmessage", "to :" + chatMessageParams.getToName() + "name :" + chatMessageParams.getName().toString());
                            membersReceiverList.add(chatMessageParams);
                            // here you can access to name property like university.name
                        }
                        reecentChatListAdapter = new RecentChatListAdapter(getActivity(), membersReceiverList);
                        listUserList.setAdapter(reecentChatListAdapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    listUserList.setVisibility(View.GONE);
                    relativeInfo.setVisibility(View.VISIBLE);
                    textNoNetwork.setText("No chats, Tap the Compose icon to chat");
                    imageNoNetwork.setImageResource(R.mipmap.icon_empty_post);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.floating_add)
    public void onViewClicked() {
        Intent i = new Intent(getActivity(), ChatActivity.class);
        i.putExtra("isFromFloating", true);
        getActivity().startActivity(i);

    }
}

package com.taggery.fragment;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.appindexing.Action;
import com.google.firebase.appindexing.FirebaseUserActions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.taggery.R;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.ChatMessageParams;
import com.taggery.utils.TaggeryConstants;
import com.taggery.view.CustomEditText;
import com.taggery.view.CustomTextView;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatScreenFragment extends BaseFragment {


    @BindView(R.id.text_chatuser)
    CustomTextView textChatuser;
    @BindView(R.id.chatname)
    RelativeLayout chatname;
    @BindView(R.id.recycleview_chat)
    RecyclerView recycleviewChat;
    @BindView(R.id.img_addmedia)
    ImageView imgAddmedia;
    @BindView(R.id.edit_chatinputmsg)
    CustomEditText editChatinputmsg;
    @BindView(R.id.img_send)
    ImageView imgSend;
    @BindView(R.id.relative_sendlayout)
    RelativeLayout relativeSendlayout;
    @BindView(R.id.relative_child)
    RelativeLayout relativeChild;
    @BindView(R.id.relative_parent)
    RelativeLayout relativeParent;
    Unbinder unbinder;
    @BindView(R.id.img_userpic)
    CircleImageView imgUserpic;
    @BindView(R.id.img_back)
    ImageView imgBack;

    private String chatWith;
    private PickSetup setup;
    private int chatWithUserID;
    private Bundle bundle;
    public String MESSAGES_CHILD_SENDER, MESSAGES_CHILD_RECEIVER;
    public String RECENT_LIST_CHILD_SENDER, RECENT_LIST_CHILD_RECEIVER;
    public String ONE_TO_ONE_CHAT_MES_CHILD = "ONE_TO_ONE_CHAT_MESSAGES";
    public String ONE_TO_MANY_CHAT_MES_CHILD = "ONE_TO_MANY_CHAT_MESSAGES";
    public String RECENT_CHAT = "RECENT_CHAT_MESSAGES";
    private static final String LOADING_IMAGE_URL = "https://www.google.com/images/spin-32.gif";
    private String senderUserName,senderPic;
    private String senderUniqueID, senderRoleName;
    private int senderUserID;
    private static final String MESSAGE_URL = "https://taggerynew.firebaseio.com/";
    private DatabaseReference messagesRef;
    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseRecyclerAdapter<ChatMessageParams, MessageViewHolder> mFirebaseAdapter;
    private String chatMessage;
    private LinearLayoutManager mLinearLayoutManager;
    private boolean isForGroundExist, isTransparentModeOn;
    private static final int REQUEST_IMAGE = 1;
    private String imageUrl, friendfcmtoken, userimage;
    private String downloadChatImageUrl;

    @Inject
    SharedPreferences sharedPreferences;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat_screen, container, false);
        unbinder = ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setStackFromEnd(true);
        senderUserName = sharedPreferences.getString(TaggeryConstants.USER_NAME, "");
        senderPic = sharedPreferences.getString(TaggeryConstants.USER_PROFILE_PIC, "");
        senderUserID = sharedPreferences.getInt(TaggeryConstants.USER_ID, 0);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        bundle = getArguments();
        if (getArguments() != null) {
            chatWith = bundle.getString(TaggeryConstants.CHAT_WITH);
            chatWithUserID = bundle.getInt(TaggeryConstants.CHATWITH_USERID);
            friendfcmtoken = bundle.getString(TaggeryConstants.CHAT_FCMTOKEN);
            userimage = bundle.getString(TaggeryConstants.CHAT_USERPIC);
            textChatuser.setText(chatWith);
            Picasso.get().load(getActivity().getString(R.string.cloudinary_base_url) + getActivity().getString(R.string.cloudinary_download_profile_picture) + userimage)
                    .placeholder(R.mipmap.icon_profile_place_holder).error(R.mipmap.icon_profile_place_holder).into(imgUserpic);
            //My node
            MESSAGES_CHILD_SENDER = senderUserID + "to" + chatWithUserID;
            //Receiver node
            MESSAGES_CHILD_RECEIVER = chatWithUserID + "to" + senderUserID;
            //Inbox node
            RECENT_LIST_CHILD_SENDER = "" + senderUserID;
            //Sent node
            RECENT_LIST_CHILD_RECEIVER = "" + chatWithUserID;
            messagesRef = mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).
                    child(MESSAGES_CHILD_SENDER);
            mFirebaseDatabaseReference.child("IS_FORE_GROUND").push().setValue(RECENT_LIST_CHILD_SENDER);
        }
        SnapshotParser<ChatMessageParams> parser = new SnapshotParser<ChatMessageParams>() {
            @Override
            public ChatMessageParams parseSnapshot(DataSnapshot dataSnapshot) {
                ChatMessageParams chatMessageParams = dataSnapshot.getValue(ChatMessageParams.class);
                if (chatMessageParams != null) {
                    chatMessageParams.setId(dataSnapshot.getKey());
                }
                return chatMessageParams;
            }
        };
        FirebaseRecyclerOptions<ChatMessageParams> options =
                new FirebaseRecyclerOptions.Builder<ChatMessageParams>()
                        .setQuery(messagesRef, parser)
                        .build();
        mFirebaseAdapter = new FirebaseRecyclerAdapter<ChatMessageParams, MessageViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull MessageViewHolder viewHolder, final int position, @NonNull ChatMessageParams chatMessageParams) {

                if (chatMessageParams.getText() != null) {
                    chatMessage = chatMessageParams.getText();
                } else if (chatMessageParams.getImageUrl() != null) {
                    imageUrl = chatMessageParams.getImageUrl();
                    if (imageUrl.startsWith("gs://")) {
                        StorageReference storageReference = FirebaseStorage.getInstance()
                                .getReferenceFromUrl(imageUrl);
                        storageReference.getDownloadUrl().addOnCompleteListener(
                                new OnCompleteListener<Uri>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Uri> task) {
                                        if (task.isSuccessful()) {
                                            downloadChatImageUrl = task.getResult().toString();
                                        } else {
                                            Log.w("chat", "Getting download url was not successful.",
                                                    task.getException());
                                        }
                                    }
                                });
                    } else {
                        downloadChatImageUrl = imageUrl;
                    }
                }

                if (chatMessageParams.getName().equalsIgnoreCase(senderUserName)) {
                    viewHolder.relative_left.setVisibility(View.GONE);
                    viewHolder.relative_right.setVisibility(View.VISIBLE);
                    viewHolder.textSentMesssgeRight.setText(chatMessageParams.getSentTime());
                    if (chatMessageParams.getText() != null) {
                        viewHolder.text_sent_imagetext.setVisibility(View.GONE);
                        viewHolder.messageImageViewRight.setVisibility(View.GONE);
                        viewHolder.textMesssgeRight.setVisibility(View.VISIBLE);
                        viewHolder.textMesssgeRight.setText(chatMessage);
                    } else if (chatMessageParams.getImageUrl() != null) {
                        viewHolder.messageImageViewRight.setVisibility(View.VISIBLE);
                        if (chatMessageParams.getImageText()!=null){
                            viewHolder.text_sent_imagetext.setVisibility(View.VISIBLE);
                            viewHolder.text_sent_imagetext.setText(chatMessageParams.getImageText());
                        }
                        viewHolder.textMesssgeRight.setVisibility(View.GONE);
                        Glide.with(viewHolder.messageImageViewRight.getContext())
                                .load(downloadChatImageUrl)
                                .into(viewHolder.messageImageViewRight);
                    }
                } else {
                    viewHolder.relative_left.setVisibility(View.VISIBLE);
                    viewHolder.relative_right.setVisibility(View.GONE);
                    viewHolder.textSentMesssgeLeft.setText(chatMessageParams.getSentTime());
                    if (chatMessageParams.getText() != null) {
                        viewHolder.messageImageViewLeft.setVisibility(View.GONE);
                        viewHolder.textMesssgeLeft.setVisibility(View.VISIBLE);
                        viewHolder.textMesssgeLeft.setText(chatMessage);
                    } else if (chatMessageParams.getImageUrl() != null) {
                        viewHolder.messageImageViewLeft.setVisibility(View.VISIBLE);
                        viewHolder.textMesssgeLeft.setVisibility(View.GONE);
                        if (chatMessageParams.getImageText()!=null){
                            viewHolder.text_sent_imagetextleft.setVisibility(View.VISIBLE);
                            viewHolder.text_sent_imagetextleft.setText(chatMessageParams.getImageText());
                        }
                        Glide.with(viewHolder.messageImageViewLeft.getContext())
                                .load(downloadChatImageUrl)
                                .into(viewHolder.messageImageViewLeft);
                    }

                }
                // log a view action on it
                FirebaseUserActions.getInstance().end(getMessageViewAction(chatMessageParams));
                viewHolder.messageImageViewLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.alert_detail_image_view, null);
                        dialogBuilder.setView(dialogView);
                        ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_detail_view);
                        AlertDialog alertDialog = dialogBuilder.create();
                        Glide.with(imageView.getContext())
                                .load(mFirebaseAdapter.getItem(position).getImageUrl())
                                .into(imageView);
                        alertDialog.show();
                    }
                });
                viewHolder.messageImageViewRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.alert_detail_image_view, null);
                        dialogBuilder.setView(dialogView);
                        ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_detail_view);
                        AlertDialog alertDialog = dialogBuilder.create();
                        Glide.with(imageView.getContext())
                                .load(mFirebaseAdapter.getItem(position).getImageUrl())
                                .into(imageView);
                        alertDialog.show();
                    }
                });


            }

            @NonNull
            @Override
            public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
                //  showProgress();
                return new MessageViewHolder(inflater.inflate(R.layout.childlayout_chatmessage,
                        viewGroup, false));
            }
        };
        mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mFirebaseAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the user is at the bottom of the list, scroll
                // to the bottom of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) && lastVisiblePosition == (positionStart - 1))) {
                    recycleviewChat.scrollToPosition(positionStart);
                }
            }
        });
        recycleviewChat.setLayoutManager(mLinearLayoutManager);
        recycleviewChat.setAdapter(mFirebaseAdapter);

        setup = new PickSetup()
                .setTitle(getString(R.string.choose_photo))
                .setProgressText(getString(R.string.processing))
                .setCancelText(getString(R.string.cancel))
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText(getString(R.string.camera))
                .setGalleryButtonText(getString(R.string.gallery))
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setGalleryIcon(R.mipmap.icon_gallery)
                .setCameraIcon(R.mipmap.icon_camera).setWidth(100).setHeight(100);


        return view;
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView textMesssgeLeft, textMesssgeRight,text_sent_imagetext;
        TextView textSentMesssgeLeft, textSentMesssgeRight,text_sent_imagetextleft;
        ImageView messageImageViewLeft, messageImageViewRight;
        RelativeLayout relative_left;
        LinearLayout relative_right;

        public MessageViewHolder(View v) {
            super(v);
            textMesssgeLeft = (TextView) itemView.findViewById(R.id.text_chat_mes_left);
            textMesssgeRight = (TextView) itemView.findViewById(R.id.text_chat_mes_right);
            textSentMesssgeRight = (TextView) itemView.findViewById(R.id.text_sent_time_right);
            textSentMesssgeLeft = (TextView) itemView.findViewById(R.id.text_sent_time_left);
            messageImageViewLeft = (ImageView) itemView.findViewById(R.id.image_chat_left);
            messageImageViewRight = (ImageView) itemView.findViewById(R.id.image_chat_right);
            text_sent_imagetext = itemView.findViewById(R.id.text_sent_imagetext);
            text_sent_imagetextleft = itemView.findViewById(R.id.text_sent_imagetextleft);
            relative_left = itemView.findViewById(R.id.relative_left);
            relative_right = itemView.findViewById(R.id.relative_right);
        }
    }

    private Action getMessageViewAction(ChatMessageParams chatMessageParams) {
        return new Action.Builder(Action.Builder.VIEW_ACTION)
                .setObject(chatMessageParams.getName(), MESSAGE_URL.concat(chatMessageParams.getId()))
                .setMetadata(new Action.Metadata.Builder().setUpload(false))
                .build();
    }

    private void sendMessage() {

        if (editChatinputmsg.getText() != null && editChatinputmsg.getText().toString().trim().length() > 0) {
            //One to One
            //Chat list
            ChatMessageParams chatMessageParams = new
                    ChatMessageParams(senderUserID, editChatinputmsg.getText().toString(), senderUserName,
                    senderPic, null, getCurrentTimeandDate(), chatWith, chatWithUserID,
                    "", "", ""
                    , "", "", friendfcmtoken, userimage, 1,null);
            //My node
            mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).push().
                    setValue(chatMessageParams);
            //Receiver node
            mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).push().
                    setValue(chatMessageParams);

            //notification
            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Notifications").push()
                    .setValue(chatMessageParams);

            //Inbox list
            final ChatMessageParams chatMessageParams3 = new
                    ChatMessageParams(senderUserID, editChatinputmsg.getText().toString().trim(), senderUserName,
                    senderPic,
                    null, getCurrentTimeandDate(), chatWith, chatWithUserID,
                    "", "", "",
                    "", "", friendfcmtoken, userimage, 1,null);
            final ChatMessageParams chatMessageParams4 = new
                    ChatMessageParams(chatWithUserID, editChatinputmsg.getText().toString().trim(), chatWith,
                    userimage,
                    null, getCurrentTimeandDate(), senderUserName, senderUserID,
                    "", "", "",
                    "", "", friendfcmtoken, senderPic, 0,null);


            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox")
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {

                            //Old User
                            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                    child(RECENT_LIST_CHILD_RECEIVER)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(final DataSnapshot snapshot) {
                                            //Old User
                                            for (final DataSnapshot dataSnapshot : snapshot.getChildren()) {
                                                mFirebaseDatabaseReference.child("IS_FORE_GROUND").
                                                        addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot dataSnapshot1) {
                                                                for (final DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                                                                    if (dataSnapshot2.getValue().equals(RECENT_LIST_CHILD_RECEIVER)) {
                                                                        isForGroundExist = true;
                                                                    } else {
                                                                        isForGroundExist = false;
                                                                    }
                                                                    break;
                                                                }
                                                                if (!isForGroundExist) {
                                                                    dataSnapshot.getRef().child("readStatus").setValue(0);
                                                                }
                                                            }

                                                            @Override
                                                            public void onCancelled(DatabaseError databaseError) {

                                                            }
                                                        });
                                                break;
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                            //                                        New
                            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").child(RECENT_LIST_CHILD_SENDER).
                                    child(RECENT_LIST_CHILD_RECEIVER).
                                    setValue(chatMessageParams3);
                            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").child(RECENT_LIST_CHILD_RECEIVER).
                                    child(RECENT_LIST_CHILD_SENDER).
                                    setValue(chatMessageParams4);


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


        } else {
            editChatinputmsg.setError("empty text");
        }
        editChatinputmsg.setText("");
    }

    public String getCurrentTimeandDate() {
        Date currentDate = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        String currentTimeandTime = simpleDateFormat.format(currentDate);
        return currentTimeandTime;
    }


    private void getImageResult(final Uri uri, final String imageText) {
        if (uri != null) {
            Log.d("chat", "Uri: " + uri.toString());


            //One to one
            ChatMessageParams tempMessage = new ChatMessageParams(senderUserID, null, senderUserName,
                    senderPic, LOADING_IMAGE_URL, getCurrentTimeandDate(), chatWith, chatWithUserID,
                    "", senderRoleName, "", "", "", friendfcmtoken,
                    userimage, 0,imageText);
            ChatMessageParams tempMessage2 = new
                    ChatMessageParams(chatWithUserID, null, chatWith,
                    senderPic,
                    LOADING_IMAGE_URL, getCurrentTimeandDate(), senderUserName, senderUserID,
                    "", "", "",
                    "", "", friendfcmtoken, userimage, 0,imageText);


            mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).push()
                    .setValue(tempMessage, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError,
                                               DatabaseReference databaseReference) {
                            if (databaseError == null) {
                                String key = databaseReference.getKey();
                                StorageReference storageReference =
                                        FirebaseStorage.getInstance()
                                                .getReference()
                                                .child(key)
                                                .child(uri.getLastPathSegment());

                                putImageInStorage1(storageReference, uri, key,imageText);
                            } else {
                                Log.w("chat", "Unable to write message to database.",
                                        databaseError.toException());
                            }
                        }
                    });
            mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).push()
                    .setValue(tempMessage2, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError,
                                               DatabaseReference databaseReference) {
                            if (databaseError == null) {
                                String key = databaseReference.getKey();
                                StorageReference storageReference =
                                        FirebaseStorage.getInstance()
                                                .getReference()
                                                .child(key)
                                                .child(uri.getLastPathSegment());

                                putImageInStorage2(storageReference, uri, key);
                            } else {
                                Log.w("chat", "Unable to write message to database.",
                                        databaseError.toException());
                            }
                        }
                    });
            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").child(RECENT_LIST_CHILD_SENDER).
                    child(RECENT_LIST_CHILD_RECEIVER).
                    setValue(tempMessage);
            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").child(RECENT_LIST_CHILD_RECEIVER).
                    child(RECENT_LIST_CHILD_SENDER).
                    setValue(tempMessage2);
            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Notifications").push()
                    .setValue(tempMessage);

        }
    }

    private void putImageInStorage1(final StorageReference storageReference, Uri uri, final String key, final String imageText) {
        storageReference.putFile(uri).
                continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    ChatMessageParams chatMessageParams =
                            new ChatMessageParams(senderUserID, null, senderUserName, senderPic,
                                    downloadUri.toString(),
                                    getCurrentTimeandDate(), chatWith, chatWithUserID, "",
                                    senderRoleName, "", "", "", friendfcmtoken,
                                    userimage, 0,imageText);
                    mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).child(key)
                            .setValue(chatMessageParams);
//                    //Sent items
//                    ChatMessageParams chatMessageParams2 = new
//                            ChatMessageParams(0, "", "",
//                            "", null, getCurrentTimeandDate(), chatWith, chatWithUserID, "",
//                            "", ""
//                            , memberID, designation, memberFCMToken, chatwithProPic);
//                    mFirebaseDatabaseReference.child(RECENT_CHAT).child("Sent").
//                            child(RECENT_LIST_CHILD_SENDER).push().
//                            setValue(chatMessageParams2);

                } else {
                    Log.w("chat", "Image upload task was not successful.",
                            task.getException());
                }
            }
        });
    }

    private void putImageInStorage2(final StorageReference storageReference, Uri uri, final String key) {
        storageReference.putFile(uri).
                continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    ChatMessageParams chatMessageParams =
                            new ChatMessageParams(senderUserID, null, senderUserName, senderPic,
                                    downloadUri.toString(), getCurrentTimeandDate(), chatWith, chatWithUserID,
                                    "", senderRoleName, ""
                                    , "", "", friendfcmtoken, userimage, 0,null);

                    mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).child(key)
                            .setValue(chatMessageParams);
                    //Inbox
                    final ChatMessageParams chatMessageParams3 = new
                            ChatMessageParams(senderUserID, "", senderUserName,
                            senderPic,
                            null, getCurrentTimeandDate(), "", 0,
                            "", senderRoleName, "",
                            "", "", friendfcmtoken, userimage, 0,null);

                    mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox")
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot snapshot) {
                                    if (snapshot.hasChild(RECENT_LIST_CHILD_RECEIVER)) {
                                        //Old User
                                        mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                                child(RECENT_LIST_CHILD_RECEIVER)
                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot snapshot) {
                                                        //Old User
                                                        for (final DataSnapshot dataSnapshot : snapshot.getChildren()) {
                                                            if (chatMessageParams3.getUniqueID().
                                                                    equals(dataSnapshot.child("uniqueID").
                                                                            getValue())) {
                                                                //Check chat with user is foreground or background
                                                                mFirebaseDatabaseReference.child("IS_FORE_GROUND").
                                                                        addListenerForSingleValueEvent(new ValueEventListener() {
                                                                            @Override
                                                                            public void onDataChange(DataSnapshot dataSnapshot1) {
                                                                                for (final DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                                                                                    if (dataSnapshot2.getValue().equals(RECENT_LIST_CHILD_RECEIVER)) {
                                                                                        isForGroundExist = true;
                                                                                    } else {
                                                                                        isForGroundExist = false;
                                                                                    }
                                                                                    break;
                                                                                }
                                                                                if (!isForGroundExist) {
                                                                                    dataSnapshot.getRef().child("readStatus").setValue(0);
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onCancelled(DatabaseError databaseError) {

                                                                            }
                                                                        });
                                                                break;
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                });
                                    } else {
//                                        New
                                        mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                                child(RECENT_LIST_CHILD_RECEIVER).push().
                                                setValue(chatMessageParams3);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                } else {
                    Log.w("chat", "Image upload task was not successful.",
                            task.getException());
                }
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.img_addmedia, R.id.img_send,R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_addmedia:
                PickImageDialog.build(setup)
                        .setOnPickResult(new IPickResult() {
                            @Override
                            public void onPickResult(PickResult r) {
                                if (r.getError() == null) {

                                    showDialog(r.getBitmap(),r.getUri());

                                } else {
                                    //Handle possible errors
                                    //TODO: do what you have to do with r.getError();
                                    Toast.makeText(getActivity(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                                }

                            }
                        })
                        .setOnPickCancel(new IPickCancel() {
                            @Override
                            public void onCancelClick() {
                            }
                        }).show(getActivity().getSupportFragmentManager());
                break;
            case R.id.img_send:
                sendMessage();
                break;
            case R.id.img_back:
                getActivity().finish();
                break;
        }
    }

    private void showDialog(Bitmap bitmap, final Uri uri) {
        ImageView img_back,img_send,img_viewfull;
        CircleImageView img_userpic;
        CustomTextView text_chatuser;
        final CustomEditText edit_chatinputmsg;

        final Dialog dialog = new Dialog(getActivity(),android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_alertdialog);
        img_send  = dialog.findViewById(R.id.img_send);
        img_viewfull  = dialog.findViewById(R.id.img_viewfull);
        img_back = dialog.findViewById(R.id.img_back);
        text_chatuser = dialog.findViewById(R.id.text_chatuser);
        img_userpic = dialog.findViewById(R.id.img_userpic);
        edit_chatinputmsg = dialog.findViewById(R.id.edit_chatinputmsg);
        img_viewfull.setImageBitmap(bitmap);



        text_chatuser.setText(chatWith);
        Picasso.get().load(getActivity().getString(R.string.cloudinary_base_url) + getActivity().getString(R.string.cloudinary_download_profile_picture) + userimage)
                .placeholder(R.mipmap.icon_profile_place_holder).error(R.mipmap.icon_profile_place_holder).into(img_userpic);

        img_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getImageResult(uri, edit_chatinputmsg.getText().toString().trim());
                dialog.dismiss();
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public void onResume() {
        super.onResume();
        mFirebaseAdapter.startListening();
    }

    @Override
    public void onPause() {
        mFirebaseAdapter.stopListening();
        mFirebaseDatabaseReference.child("IS_FORE_GROUND").
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot1) {
                        for (final DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                            if (dataSnapshot2.getValue().equals(RECENT_LIST_CHILD_SENDER)) {
                                dataSnapshot2.getRef().removeValue();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        super.onPause();
    }
}

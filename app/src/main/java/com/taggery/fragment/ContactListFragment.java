package com.taggery.fragment;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.taggery.R;
import com.taggery.activity.BaseActivity;
import com.taggery.activity.MainActivity;
import com.taggery.activity.RunTimePermission;
import com.taggery.adapter.ContactListAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.ContactListInputParam;
import com.taggery.model.ContactListResponse;
import com.taggery.model.SuggestionItem;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactListFragment extends BaseFragment {

    @BindView(R.id.image_app_logo)
    ImageView imageAppLogo;
    @BindView(R.id.text_label_contact_title)
    CustomTextView textLabelContactTitle;
    @BindView(R.id.list_contacts)
    ListView listContacts;
    @BindView(R.id.cons_contact_list)
    ConstraintLayout consContactList;
    Unbinder unbinder;
    ContactListAdapter contactListAdapter;
    @BindView(R.id.image_warn_icon)
    ImageView imageWarnIcon;
    @BindView(R.id.text_warn)
    CustomTextView textWarn;
    @BindView(R.id.cons_warn)
    ConstraintLayout consWarn;
    @BindView(R.id.text_post)
    CustomTextView textPost;
    @BindView(R.id.img_post)
    ImageView imgPost;
    @BindView(R.id.relative_next)
    RelativeLayout relativeNext;
    @BindView(R.id.swipe_contact_list)
    SwipeRefreshLayout swipeContactList;
    private RunTimePermission runTimePermission;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    TaggeryAPI taggeryAPI;
    private ArrayList<String> contactLists;
    private ArrayList<SuggestionItem> suggestionItemArrayList;
    private ContactListResponse contactListResponse;
    private BaseActivity loginActivity;
    private boolean isLoading = false, reachedLastEnd = false;
    private int paginationCount = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        contactLists = new ArrayList<String>();
        loginActivity = (BaseActivity) getActivity();
        suggestionItemArrayList = new ArrayList<SuggestionItem>();
        contactListAdapter = new ContactListAdapter(getActivity(), suggestionItemArrayList, loginActivity);
        listContacts.setAdapter(contactListAdapter);
        runTimePermission = new RunTimePermission(getActivity());
        runTimePermission.requestPermission(new String[]{Manifest.permission.CAMERA,
                Manifest.permission.READ_CONTACTS
        }, new RunTimePermission.RunTimePermissionListener() {

            @Override
            public void permissionGranted() {
                // First we need to check availability of play services
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        showProgress();
                        readContacts();
                    }
                }).start();
            }

            @Override
            public void permissionDenied() {
            }
        });

        swipeContactList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeContactList.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showProgress();
                        if (contactLists != null && contactLists.size() != 0)
                            getContactList(1);
                        else
                            readContacts();
                        swipeContactList.setRefreshing(false);
                    }
                }, 000);
            }
        });


        listContacts.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (!isLoading && !reachedLastEnd) {
                    if ((visibleItemCount + firstVisibleItem) >= totalItemCount
                            && firstVisibleItem >= 0
                            && totalItemCount >= 29) {
                        getContactList(++paginationCount);
                    }
                }
            }
        });

        return view;
    }

    private void readContacts() {
        ContentResolver cr = getActivity().getContentResolver();
        String orderBy = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC";
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, orderBy);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contactLists.add(phoneNo.trim());
                    }
                    pCur.close();
                }
            }
        }
        Set<String> unique = new LinkedHashSet<String>(contactLists);
        contactLists = new ArrayList<String>(unique);
        Log.i("phoneno", contactLists.toString());
        getContactList(1);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isLoading = false;
        reachedLastEnd = false;
        unbinder.unbind();
    }

    @OnClick(R.id.relative_next)
    public void onViewClicked() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void getContactList(final int paginationCount) {
        if (Util.isNetworkAvailable()) {
            isLoading = true;
            if (paginationCount == 1)
                suggestionItemArrayList.clear();
            ContactListInputParam contactListInputParam = new ContactListInputParam();
            contactListInputParam.setNumbers(contactLists);
            contactListInputParam.setPage(paginationCount);
            contactListInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            taggeryAPI.getContactList(contactListInputParam).enqueue(new Callback<ContactListResponse>() {
                @Override
                public void onResponse(Call<ContactListResponse> call, Response<ContactListResponse> response) {
                    hideProgress();
                    isLoading = false;
                    contactListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (contactListResponse.getStatusCode() == 200) {
                            if (contactListResponse.getSuggestion().size() != 0) {
                                listContacts.setVisibility(View.VISIBLE);
                                consWarn.setVisibility(View.GONE);
                                if (contactListResponse.getSuggestion() != null)
                                    suggestionItemArrayList.addAll(contactListResponse.getSuggestion());
                                contactListAdapter.notifyDataSetChanged();

                            } else {
                                listContacts.setVisibility(View.GONE);
                                consWarn.setVisibility(View.VISIBLE);
                                textWarn.setText(getString(R.string.no_contacts));
                                imageWarnIcon.setImageResource(R.mipmap.icon_no_contacts);
                            }

                        } else if (contactListResponse.getStatusCode() == 501) {
                            if (paginationCount != 1) {
                                reachedLastEnd = true;
                                showShackError(contactListResponse.getMessage()!=null?
                                        contactListResponse.getMessage():"", consContactList);
                            } else {
                                listContacts.setVisibility(View.GONE);
                                consWarn.setVisibility(View.VISIBLE);
                                textWarn.setText(getString(R.string.server_error));
                                imageWarnIcon.setImageResource(R.mipmap.icon_no_contacts);
                            }
                        } else {
                            listContacts.setVisibility(View.GONE);
                            consWarn.setVisibility(View.VISIBLE);
                            textWarn.setText(getString(R.string.server_error));
                            imageWarnIcon.setImageResource(R.mipmap.icon_no_contacts);
                        }

                    } else {
                        listContacts.setVisibility(View.GONE);
                        consWarn.setVisibility(View.VISIBLE);
                        textWarn.setText(getString(R.string.server_error));
                        imageWarnIcon.setImageResource(R.mipmap.icon_no_contacts);
                    }
                }

                @Override
                public void onFailure(Call<ContactListResponse> call, Throwable t) {
                    hideProgress();
                    isLoading = false;
                    listContacts.setVisibility(View.GONE);
                    consWarn.setVisibility(View.VISIBLE);
                    textWarn.setText(getString(R.string.server_error));
                    imageWarnIcon.setImageResource(R.mipmap.icon_no_contacts);
                }
            });

        } else {
            listContacts.setVisibility(View.GONE);
            consWarn.setVisibility(View.VISIBLE);
            textWarn.setText(getString(R.string.no_network));
            imageWarnIcon.setImageResource(R.mipmap.icon_no_network);
        }
    }
}

package com.taggery.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.taggery.R;
import com.taggery.view.CustomTextView;
import com.taggery.view.CustomTextViewBold;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PrivacySettingsFragment extends BaseFragment {

    @BindView(R.id.img_app_logo)
    ImageView imgAppLogo;
    @BindView(R.id.text_label_settings)
    CustomTextViewBold textLabelSettings;
    @BindView(R.id.relative_settings)
    RelativeLayout relativeSettings;
    @BindView(R.id.text_notification)
    CustomTextView textNotification;
    @BindView(R.id.switch_notification_off)
    SwitchCompat switchNotificationOff;
    @BindView(R.id.relative_notifications)
    RelativeLayout relativeNotifications;
    @BindView(R.id.text_deactivate_likes)
    CustomTextView textDeactivateLikes;
    @BindView(R.id.switch_deactivate_likes)
    SwitchCompat switchDeactivateLikes;
    @BindView(R.id.relative_deactivate_likes)
    RelativeLayout relativeDeactivateLikes;
    @BindView(R.id.cons_private_settings)
    ConstraintLayout consPrivateSettings;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_private_settings, container, false);
        unbinder = ButterKnife.bind(this, view);


        switchDeactivateLikes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });

        switchNotificationOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

package com.taggery.model;

public class AddComment {

    public int getCommentUserID() {
        return CommentUserID;
    }

    public void setCommentUserID(int commentUserID) {
        CommentUserID = commentUserID;
    }

    public int getCommentPostID() {
        return CommentPostID;
    }

    public void setCommentPostID(int commentPostID) {
        CommentPostID = commentPostID;
    }

    public String getCommentText() {
        return CommentText;
    }

    public void setCommentText(String commentText) {
        CommentText = commentText;
    }

    private int CommentUserID;
    private int CommentPostID;
    private String CommentText;
}

package com.taggery.model;

import java.util.List;

public class AddpostInputParam {

    public int getPostUserID() {
        return PostUserID;
    }

    public void setPostUserID(int postUserID) {
        PostUserID = postUserID;
    }

    public int getPostType() {
        return PostType;
    }

    public void setPostType(int postType) {
        PostType = postType;
    }

    public String getPostText() {
        return PostText;
    }

    public void setPostText(String postText) {
        PostText = postText;
    }

    public String getPostImage() {
        return PostImage;
    }

    public void setPostImage(String postImage) {
        PostImage = postImage;
    }

    public String getPostLocation() {
        return PostLocation;
    }

    public void setPostLocation(String postLocation) {
        PostLocation = postLocation;
    }

    public Double getPostLatitude() {
        return PostLatitude;
    }

    public void setPostLatitude(Double postLatitude) {
        PostLatitude = postLatitude;
    }

    public Double getPostLongitude() {
        return PostLongitude;
    }

    public void setPostLongitude(Double postLongitude) {
        PostLongitude = postLongitude;
    }

    public int getPostTimeFrame() {
        return PostTimeFrame;
    }

    public void setPostTimeFrame(int postTimeFrame) {
        PostTimeFrame = postTimeFrame;
    }

    public List<Integer> getPostTagList() {
        return PostTagList;
    }

    public void setPostTagList(List<Integer> postTagList) {
        PostTagList = postTagList;
    }

    private int PostUserID;
    private int PostType;

    public int getMediaType() {
        return mediaType;
    }

    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }

    private int mediaType;
    private String PostText;
    private String PostImage;
    private String PostLocation;
    private Double PostLatitude;
    private Double PostLongitude;
    private int PostTimeFrame;
    private List<Integer> PostTagList;

    public String getPostTime() {
        return PostTime;
    }

    public void setPostTime(String postTime) {
        PostTime = postTime;
    }

    private String PostTime;

}

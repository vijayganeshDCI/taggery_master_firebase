package com.taggery.model;

public class AnalyticalListItem{
	public int getNotification_caption() {
		return notification_caption;
	}

	public void setNotification_caption(int notification_caption) {
		this.notification_caption = notification_caption;
	}

	public String getNotification_postImage() {
		return notification_postImage;
	}

	public void setNotification_postImage(String notification_postImage) {
		this.notification_postImage = notification_postImage;
	}

	public int getNotification_postImageType() {
		return notification_postImageType;
	}

	public void setNotification_postImageType(int notification_postImageType) {
		this.notification_postImageType = notification_postImageType;
	}

	public String getNotification_created_UserPicture() {
		return notification_created_UserPicture;
	}

	public void setNotification_created_UserPicture(String notification_created_UserPicture) {
		this.notification_created_UserPicture = notification_created_UserPicture;
	}

	public String getNotification_text() {
		return notification_text;
	}

	public void setNotification_text(String notification_text) {
		this.notification_text = notification_text;
	}

	private int notification_caption;

	public int getNotification_created_UserID() {
		return notification_created_UserID;
	}

	public void setNotification_created_UserID(int notification_created_UserID) {
		this.notification_created_UserID = notification_created_UserID;
	}

	private int notification_created_UserID;
	private String notification_postImage;
	private int notification_postImageType;
	private String notification_created_UserPicture;
	private String notification_text;

	public String getNotification_created_at() {
		return notification_created_at;
	}

	public void setNotification_created_at(String notification_created_at) {
		this.notification_created_at = notification_created_at;
	}

	private String notification_created_at;

	public int getUserTotalLikesCount() {
		return UserTotalLikesCount;
	}

	public void setUserTotalLikesCount(int userTotalLikesCount) {
		UserTotalLikesCount = userTotalLikesCount;
	}

	private int UserTotalLikesCount;
}

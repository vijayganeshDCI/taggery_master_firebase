package com.taggery.model;

/**
 * Created by keerthana on 9/5/2018.
 */

public class Analyticalpojo {
    private String img, but, title,subtitle;

    public Analyticalpojo() {
    }

    public Analyticalpojo(String img, String but, String title,String subtitle) {
        this.img = img;
        this.but = but;
        this.title = title;
        this.subtitle=subtitle;

    }

    public String getBut() {
        return but;
    }

    public void setBut(String but) {
        this.but = but;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

}
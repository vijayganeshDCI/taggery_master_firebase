package com.taggery.model;

public class ContactList {
    public int getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(int profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ContactList(int profilePicture, String name,String mobile) {
        this.profilePicture = profilePicture;
        this.name = name;
        this.mobile =mobile;
    }

    private int profilePicture;
    private String name;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    private String mobile;

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;

        if (!(obj instanceof ContactList)) {
            return false;
        }


        return this.name == ((ContactList) obj).getName();
    }

    @Override
    public int hashCode() {
        return (name == null) ? 0 : name.hashCode();
    }

}



package com.taggery.model;

import android.graphics.Bitmap;


public class FilterThumModel {
    public String filterName;
    public String imageurl;
    public String thumb;

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }



    public FilterThumModel(String filterName, String imageurl ,String thumb) {
        this.filterName = filterName;
        this.imageurl = imageurl;
        this.thumb = thumb;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
}

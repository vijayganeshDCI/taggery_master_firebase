package com.taggery.model;

public class LikeList {

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public LikeList(String userName, int userID) {
        this.userName = userName;
        this.userID = userID;
    }

    private String userName;
    private int userID;
}

package com.taggery.model;

public class LoginResponse {
    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public com.taggery.model.UserDetails getUserDetails() {
        return UserDetails;
    }

    public void setUserDetails(com.taggery.model.UserDetails userDetails) {
        UserDetails = userDetails;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    private String Status;
    private UserDetails UserDetails;
    private String Message;
    private int StatusCode;
}

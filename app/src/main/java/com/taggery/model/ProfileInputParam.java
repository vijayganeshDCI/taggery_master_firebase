package com.taggery.model;

public class ProfileInputParam  {
    public int getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(int owner_id) {
        this.owner_id = owner_id;
    }

    public int owner_id;

    public int getViewer_id() {
        return viewer_id;
    }

    public void setViewer_id(int viewer_id) {
        this.viewer_id = viewer_id;
    }

    public int viewer_id;
    public int page;




    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }





}

package com.taggery.model;

public class SuggestionItem {


    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserPicture() {
        return UserPicture;
    }

    public void setUserPicture(String userPicture) {
        UserPicture = userPicture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String UserName;
    private String UserPicture;

    public SuggestionItem(String userPicture, int id,
                          String userFirstName, String userLastName,String status,int UserTotalLikesCount ) {
        UserPicture = userPicture;
        this.id = id;
        UserFirstName = userFirstName;
        UserLastName = userLastName;
        this.Status=status;
        this.UserTotalLikesCount=UserTotalLikesCount;

    }

    private int id;

    public String getUserFirstName() {
        return UserFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        UserFirstName = userFirstName;
    }

    public String getUserLastName() {
        return UserLastName;
    }

    public void setUserLastName(String userLastName) {
        UserLastName = userLastName;
    }

    private String UserFirstName;
    private String UserLastName;

    public int getSnapID() {
        return SnapID;
    }

    public void setSnapID(int snapID) {
        SnapID = snapID;
    }

    private int SnapID;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    private String Status;

    public int getFollowing() {
        return Following;
    }

    public void setFollowing(int following) {
        Following = following;
    }

    private  int Following;


    public int getUserTotalLikesCount() {
        return UserTotalLikesCount;
    }

    public void setUserTotalLikesCount(int userTotalLikesCount) {
        UserTotalLikesCount = userTotalLikesCount;
    }

    private int UserTotalLikesCount;



}

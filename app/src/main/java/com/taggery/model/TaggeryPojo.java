package com.taggery.model;

/**
 * Created by keerthana on 9/3/2018.
 */


public class TaggeryPojo {
    private String img, but,but1, name;

    public TaggeryPojo() {
    }

    public TaggeryPojo(String img, String but, String name,String but1) {
        this.img = img;
        this.but = but;
        this.name = name;
        this.but1=but1;

    }

    public String getBut() {
        return but;
    }

    public void setBut(String but) {
        this.but = but;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBut1(){
        return but1;
    }
public void setBut1(String but1)
{
    this.but1=but1;
}
}
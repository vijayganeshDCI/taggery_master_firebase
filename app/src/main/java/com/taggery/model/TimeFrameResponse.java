package com.taggery.model;

import java.util.List;

public class TimeFrameResponse {
    public List<Integer> getTimeframe() {
        return timeframe;
    }

    public void setTimeframe(List<Integer> timeframe) {
        this.timeframe = timeframe;
    }

    private List<Integer> timeframe;
}
package com.taggery.model;

public class ViewPost {
    public int getViewUserID() {
        return ViewUserID;
    }

    public void setViewUserID(int viewUserID) {
        ViewUserID = viewUserID;
    }

    public int getViewPostID() {
        return ViewPostID;
    }

    public void setViewPostID(int viewPostID) {
        ViewPostID = viewPostID;
    }

    private int ViewUserID;
    private int ViewPostID;
}

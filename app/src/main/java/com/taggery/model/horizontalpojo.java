package com.taggery.model;

/**
 * Created by keerthana on 9/4/2018.
 */

public class horizontalpojo {
    private String img, text,button;

    public horizontalpojo() {
    }

    public horizontalpojo(String img, String text, String button) {
        this.img = img;
       this.text=text;
       this.button=button;

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }


}


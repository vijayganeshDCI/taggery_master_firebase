package com.taggery.retrofit;


import com.taggery.model.AddComment;
import com.taggery.model.AddLike;
import com.taggery.model.AddpostInputParam;
import com.taggery.model.AnalyticalListResponse;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.CommentListResponse;
import com.taggery.model.ContactListInputParam;
import com.taggery.model.ContactListResponse;
import com.taggery.model.FriendRequestInputParam;
import com.taggery.model.GetPostDetailResponse;
import com.taggery.model.LoginResponse;
import com.taggery.model.ProfileInputParam;
import com.taggery.model.ProfileResponse;
import com.taggery.model.RegistrationParam;
import com.taggery.model.RegistrationResponse;
import com.taggery.model.SnapInputParam;
import com.taggery.model.SnapResponse;
import com.taggery.model.TagInputParam;
import com.taggery.model.TagPeopleResponse;
import com.taggery.model.TimeFrameResponse;
import com.taggery.model.UserProfileUploadAfter12Hrs;
import com.taggery.model.ViewPost;
import com.taggery.model.ViewPostResponse;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface TaggeryAPI {

    @POST("registration")
    public Call<RegistrationResponse> registration(@Body RegistrationParam registrationParam);

    @POST("checkusername")
    public Call<CheckUserNameResponse> checkUserName(@Body RegistrationParam registrationParam);

    @POST("login")
    public Call<LoginResponse> login(@Body RegistrationParam registrationParam);

    @POST("forgetpassword")
    public Call<CheckUserNameResponse> forgetPassword(@Body RegistrationParam registrationParam);

    @POST("changepassword")
    public Call<CheckUserNameResponse> changePassword(@Body RegistrationParam registrationParam);

    @POST("editprofile")
    public Call<LoginResponse> updateProfile(@Body RegistrationParam registrationParam);

    @POST("changeprofiletype")
    public Call<CheckUserNameResponse> updateProfileType(@Body RegistrationParam registrationParam);

    @POST("deleteaccount")
    public Call<CheckUserNameResponse> deleteAccount(@Body RegistrationParam registrationParam);

    @POST("homepage")
    public Call<SnapResponse> getSnapList(@Body SnapInputParam snapInputParam);

    @POST("addpost")
    public Call<CheckUserNameResponse> addPost(@Body AddpostInputParam addpostInputParam);

    @POST("contact")
    public Call<ContactListResponse> getContactList(@Body ContactListInputParam contactListInputParam);

    @POST("follow")
    public Call<CheckUserNameResponse> friendRequest(@Body FriendRequestInputParam friendRequestInputParam);

    @POST("reqresponce")
    public Call<CheckUserNameResponse> acceptAndRejectRequest(@Body FriendRequestInputParam friendRequestInputParam);

    @POST("getrequestlist")
    public Call<ProfileResponse> getFriendRequestList(@Body ContactListInputParam contactListInputParam);

    @POST("profile")
    public Call<ProfileResponse> profileresponse(@Body ProfileInputParam profileInputParam);

    @POST("friendslist")
    public Call<TagPeopleResponse> tagpeopleresponse(@Body TagInputParam tagInputParam);

    @GET("timeframe")
    public Call<TimeFrameResponse> getTimeFrameList();

    @POST("addlike")
    public Call<CheckUserNameResponse> addLike(@Body AddLike addLike);

    @POST("addcomments")
    public Call<CheckUserNameResponse> addComment(@Body AddComment addComment);

    @POST("getpostdetails")
    public Call<GetPostDetailResponse> getPostDetails(@Body ContactListInputParam contactListInputParam);

    @POST("viewpost")
    public Call<ViewPostResponse> viewPost(@Body ViewPost viewPost);

    @POST("report")
    public Call<CheckUserNameResponse> addReport(@Body ContactListInputParam contactListInputParam);

    @POST("getcomments")
    public Call<CommentListResponse> getCommentList(@Body ContactListInputParam contactListInputParam);

    @POST("myanalyticalpage")
    public Call<AnalyticalListResponse> getAnalyticalList(@Body ContactListInputParam contactListInputParam);

    @POST("followings")
    public Call<ContactListResponse> getFollowingList(@Body ContactListInputParam contactListInputParam);

    @POST("followers")
    public Call<ContactListResponse> getFollowerList(@Body ContactListInputParam contactListInputParam);

    @POST("locationlist")
    public Call<SnapResponse> getLocationList(@Body SnapInputParam snapInputParam);

    @POST("profilepaginate")
    public Call<ProfileResponse> getProfileMediaList(@Body ProfileInputParam profileInputParam);

    @POST("onlinestatus")
    public Call<CheckUserNameResponse> onlineStatus(@Body ContactListInputParam contactListInputParam);





}

